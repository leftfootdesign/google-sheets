## The file duplicateSheet.js is a Google script file
- It needs to be attached to a single sheet file within the sheets app
- After attaching it, you can pass in the name of the Sheet that's within the open workbook that you'd like to copy, along with the name of the sheet.
- The script will copy the sheet to the new sheet along with content, and and permissions whether at the sheet level or at the cell level.