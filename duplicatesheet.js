/*
 * This script copies a sheet along with all of it's permissions to a new sheet within a workbook
 */

/*
 * Replace between the quotes the name of the sheet you want to duplicat
 */
var copyFromName = 'Sheet2';

/*
 * Replace between the quotes the name of the sheet you want to create
 */
var copyToName = 'New-Sheet2';

function duplicateSheetWithProtections() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    copyFrom = ss.getSheetByName(copyFromName);
    copyTo = copyFrom.copyTo(ss).setName(copyToName);

    /*
     * Copy the protections set at the sheet level to a new sheet
     */
    var sheetProtections = copyFrom.getProtections(SpreadsheetApp.ProtectionType.SHEET)[0];
    if (sheetProtections) {
        var newSheetProtections = copyTo.protect();
        newSheetProtections.setDescription(sheetProtections.getDescription());
        newSheetProtections.setWarningOnly(sheetProtections.isWarningOnly());
        if (!sheetProtections.isWarningOnly()) {
            newSheetProtections.removeEditors(newSheetProtections.getEditors());
            newSheetProtections.addEditors(sheetProtections.getEditors());
            //newSheetProtections.setDomainEdit(sheetProtections.canDomainEdit()); //  only if using an Apps domain 
        }
        var ranges = sheetProtections.getUnprotectedRanges();
        var newRanges = [];
        for (var i = 0; i < ranges.length; i++) {
            newRanges.push(copyTo.getRange(ranges[i].getA1Notation()));
        }
        newSheetProtections.setUnprotectedRanges(newRanges);
    }

    /*
     * Copy the cell-level permissions within that sheet to the new sheet
     */
    var protectionRange = copyFrom.getProtections(SpreadsheetApp.ProtectionType.RANGE);
    for (var i = 0; i < protectionRange.length; i++) {
        var rangeNotation = protectionRange[i].getRange().getA1Notation();
        var pRange2 = copyTo.getRange(rangeNotation).protect();
        pRange2.setDescription(protectionRange[i].getDescription());
        pRange2.setWarningOnly(protectionRange[i].isWarningOnly());
        if (!protectionRange[i].isWarningOnly()) {
            pRange2.removeEditors(pRange2.getEditors());
            pRange2.addEditors(protectionRange[i].getEditors());
            // newSheetProtections.setDomainEdit(sheetProtections.canDomainEdit()); //  only if using an Apps domain 
        }
    }
}